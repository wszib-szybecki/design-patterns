package structural.adapter;

public class Main {

    public static void main(String[] args) {
        SquarePlug squarePlug = new SquarePlug();
        SquareElectricSocket socket = new SquareElectricSocket();

        socket.connect(squarePlug);

        RoundPlug roundPlug = new RoundPlug();
        RoundPlugAdapter adapter = new RoundPlugAdapter(roundPlug);

        socket.connect(adapter);
    }
}
