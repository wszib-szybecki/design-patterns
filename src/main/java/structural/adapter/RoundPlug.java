package structural.adapter;

public class RoundPlug {

    public String connected() {
        return "round";
    }
}
