package structural.adapter;

public class SquareElectricSocket {

    public void connect(SquarePlug plug) {
        System.out.println("Plugged " + plug.connected());
    }
}
