package structural.adapter;

public class SquarePlug {

    public String connected() {
        return "square";
    }
}
