package structural.adapter;

public class RoundPlugAdapter extends SquarePlug {

    private final RoundPlug roundPlug;

    public RoundPlugAdapter(RoundPlug roundPlug) {
        this.roundPlug = roundPlug;
    }

    @Override
    public String connected() {
        return this.roundPlug.connected();
    }
}
