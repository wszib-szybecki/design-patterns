package structural.decorator.file;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import java.security.SecureRandom;
import java.util.Base64;

public class EncryptFileUtilDecorator extends FileUtil {

    private final Cipher encryptCipher;
    private final Cipher decryptCipher;

    public EncryptFileUtilDecorator() throws Exception {
        KeyGenerator generator = KeyGenerator.getInstance("AES");
        generator.init(256);
        SecretKey key = generator.generateKey();

        byte[] iv = new byte[16];
        new SecureRandom().nextBytes(iv);
        IvParameterSpec parameterSpec = new IvParameterSpec(iv);

        this.encryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        this.encryptCipher.init(Cipher.ENCRYPT_MODE, key, parameterSpec);

        this.decryptCipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        this.decryptCipher.init(Cipher.DECRYPT_MODE, key, parameterSpec);
    }

    @Override
    public void write(String filename, String content) throws Exception {
        byte[] cipherText = this.encryptCipher.doFinal(content.getBytes());

        super.write(filename, Base64.getEncoder().encodeToString(cipherText));
    }

    @Override
    public String read(String filename) throws Exception {
        String encrypted = super.read(filename);
        byte[] plainText = this.decryptCipher.doFinal(Base64.getDecoder().decode(encrypted));

        return new String(plainText);
    }
}
