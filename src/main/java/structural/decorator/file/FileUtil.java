package structural.decorator.file;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class FileUtil {

    private final String BASE_PATH = "/home/szybek/Desktop/wszib/wzorce/lekcja2/";

    public void write(String filename, String content) throws Exception {
        Path filePath = Paths.get(BASE_PATH + filename);
        Files.writeString(filePath, content, StandardCharsets.UTF_8, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    public String read(String filename) throws Exception {
        Path filePath = Paths.get(BASE_PATH + filename);
        return Files.readString(filePath, StandardCharsets.UTF_8);
    }
}
