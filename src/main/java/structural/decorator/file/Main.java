package structural.decorator.file;

public class Main {

    public static void main(String[] args) throws Exception {
        FileUtil fileUtil = new FileUtil();
        FileUtil decorator = new EncryptFileUtilDecorator();

        Sample normal = new Sample("normal.txt", fileUtil);
        Sample encrypt = new Sample("encrypt.txt", decorator);

        normal.write("Lorem ipsum");
        encrypt.write("Lorem ipsum");

        System.out.println(normal.read());
        System.out.println(encrypt.read());
    }
}
