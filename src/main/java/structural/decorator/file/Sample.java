package structural.decorator.file;

public class Sample {

    private final String filename;
    private final FileUtil fileUtil;

    public Sample(String filename, FileUtil fileUtil) {
        this.filename = filename;
        this.fileUtil = fileUtil;
    }

    public void write(String content) throws Exception {
        this.fileUtil.write(this.filename, content);
    }

    public String read() throws Exception {
        return this.fileUtil.read(this.filename);
    }
}
