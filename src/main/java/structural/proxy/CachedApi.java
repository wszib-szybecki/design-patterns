package structural.proxy;

import java.util.HashMap;
import java.util.Map;

public class CachedApi implements Api {

    private final Api api;
    private final Map<String, String> cache;

    public CachedApi(Api api) {
        this.api = api;
        this.cache = new HashMap<>();
    }

    @Override
    public String call(String path) {
        this.cache.computeIfAbsent(path, this.api::call);

        return this.cache.get(path);
    }
}
