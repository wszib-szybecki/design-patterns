package structural.proxy;

public class Main {

    public static void main(String[] args) {
        Api clientApi = new ClientApi();
        Api cachedApi = new CachedApi(clientApi);
        Service service = new Service(cachedApi);

        service.doSomething();
        service.doSomething();
        service.doSomething();
        service.doSomething();
        service.doSomething();
        service.doSomething();
    }
}
