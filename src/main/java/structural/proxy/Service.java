package structural.proxy;

public class Service {

    private final Api api;

    public Service(Api api) {
        this.api = api;
    }

    public void doSomething() {
        System.out.println(this.api.call("/test-path"));
    }
}
