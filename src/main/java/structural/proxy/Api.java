package structural.proxy;

public interface Api {

    public String call(String path);
}
