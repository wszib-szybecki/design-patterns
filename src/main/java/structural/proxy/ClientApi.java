package structural.proxy;

public class ClientApi implements Api {

    @Override
    public String call(String path) {
        System.out.println("ClientApi calling api");
        return "Lorem ipsum";
    }
}
