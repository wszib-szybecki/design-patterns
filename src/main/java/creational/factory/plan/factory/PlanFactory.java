package creational.factory.plan.factory;

import creational.factory.plan.plan.CommercialPlan;
import creational.factory.plan.plan.IndividualPlan;
import creational.factory.plan.plan.Plan;
import creational.factory.plan.util.PlanType;

public class PlanFactory {

    public static Plan getPlan(PlanType type) {
        Plan plan = new IndividualPlan();

        if (PlanType.COMMERCIAL.equals(type)) {
            plan = new CommercialPlan();
        }

        return plan;
    }
}
