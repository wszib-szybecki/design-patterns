package creational.factory.plan;

import creational.factory.plan.factory.PlanFactory;
import creational.factory.plan.plan.Plan;
import creational.factory.plan.util.PlanType;

public class Main {

    public static void main(String[] args) {
        Plan individual = PlanFactory.getPlan(PlanType.INDIVIDUAL);
        Plan commercial = PlanFactory.getPlan(PlanType.COMMERCIAL);

        System.out.println("Individual: " + individual.getRate());
        System.out.println("Commercial: " + commercial.getRate());
    }
}
