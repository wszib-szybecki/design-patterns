package creational.factory.plan.util;

public enum PlanType {

    COMMERCIAL,
    INDIVIDUAL;
}
