package creational.factory.plan.plan;

public class CommercialPlan implements Plan {

    @Override
    public double getRate() {
        return 2.5;
    }
}
