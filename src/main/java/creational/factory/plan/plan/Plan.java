package creational.factory.plan.plan;

public interface Plan {

    double getRate();
}
