package creational.factory.plan.plan;

public class IndividualPlan implements Plan {

    @Override
    public double getRate() {
        return 1.5;
    }
}
