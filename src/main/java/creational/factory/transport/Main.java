package creational.factory.transport;

import creational.factory.transport.logistic.Logistic;
import creational.factory.transport.logistic.RoadLogistic;
import creational.factory.transport.logistic.SeaLogistic;
import creational.factory.transport.transport.Transport;

public class Main {

    public static void main(String[] args) {
        Logistic roadLogistic = new RoadLogistic();
        Transport truckTransport = roadLogistic.getTransport();

        Logistic seaLogistic = new SeaLogistic();
        Transport shipTransport = seaLogistic.getTransport();

        truckTransport.send();
        shipTransport.send();

        Company seaCompany = new Company(seaLogistic);
        seaCompany.send();
    }
}
