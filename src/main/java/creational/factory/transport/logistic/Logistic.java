package creational.factory.transport.logistic;

import creational.factory.transport.transport.Transport;

public interface Logistic {

    Transport getTransport();
}
