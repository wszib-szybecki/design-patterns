package creational.factory.transport.logistic;

import creational.factory.transport.transport.ShipTransport;
import creational.factory.transport.transport.Transport;

public class SeaLogistic implements Logistic {

    @Override
    public Transport getTransport() {
        return new ShipTransport();
    }
}
