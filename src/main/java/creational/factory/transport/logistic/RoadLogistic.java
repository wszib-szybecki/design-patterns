package creational.factory.transport.logistic;

import creational.factory.transport.transport.Transport;
import creational.factory.transport.transport.TruckTransport;

public class RoadLogistic implements Logistic {

    @Override
    public Transport getTransport() {
        return new TruckTransport();
    }
}
