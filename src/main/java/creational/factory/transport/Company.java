package creational.factory.transport;

import creational.factory.transport.logistic.Logistic;

public class Company {

    private final Logistic logistic;

    public Company(Logistic logistic) {
        this.logistic = logistic;
    }

    public void send() {
        logistic.getTransport().send();
    }
}
