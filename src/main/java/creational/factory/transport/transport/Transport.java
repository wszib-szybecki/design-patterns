package creational.factory.transport.transport;

public interface Transport {

    void send();
}
