package creational.factory.transport.transport;

public class ShipTransport implements Transport {

    @Override
    public void send() {
        System.out.println("Sending through ship");
    }
}
