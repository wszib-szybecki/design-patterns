package creational.factory.transport.transport;

public class TruckTransport implements Transport {

    @Override
    public void send() {
        System.out.println("Sending through truck");
    }
}
