package creational.abstractFactory;

import creational.abstractFactory.car.CarFactory;
import creational.abstractFactory.honda.HondaFactory;
import creational.abstractFactory.suzuki.SuzukiFactory;

public class Main {

    public static void main(String[] args) {
        CarFactory suzukiFactory = new SuzukiFactory();
        CarFactory hondaFactory = new HondaFactory();

        var suzukiDealer = new Dealer(suzukiFactory);
        var hondaDealer = new Dealer(hondaFactory);

        suzukiDealer.getCar().indentify();
        suzukiDealer.getMoto().identify();

        hondaDealer.getCar().indentify();
        hondaDealer.getMoto().identify();
    }
}
