package creational.abstractFactory.honda;

import creational.abstractFactory.car.Car;

public class Civic implements Car {

    @Override
    public void indentify() {
        System.out.println("Honda civic");
    }
}
