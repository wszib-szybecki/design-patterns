package creational.abstractFactory.honda;

import creational.abstractFactory.car.Car;
import creational.abstractFactory.car.CarFactory;
import creational.abstractFactory.car.Moto;

public class HondaFactory implements CarFactory {

    @Override
    public Car getCar() {
        return new Civic();
    }

    @Override
    public Moto getMoto() {
        return new Cbr();
    }
}
