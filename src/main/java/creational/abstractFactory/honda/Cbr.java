package creational.abstractFactory.honda;

import creational.abstractFactory.car.Moto;

public class Cbr implements Moto {

    @Override
    public void identify() {
        System.out.println("Honda CBR");
    }
}
