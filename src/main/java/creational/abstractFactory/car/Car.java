package creational.abstractFactory.car;

public interface Car {

    void indentify();
}
