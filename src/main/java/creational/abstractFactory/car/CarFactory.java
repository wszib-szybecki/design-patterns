package creational.abstractFactory.car;

public interface CarFactory {
    Car getCar();
    Moto getMoto();
}
