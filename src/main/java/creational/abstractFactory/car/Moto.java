package creational.abstractFactory.car;

public interface Moto {

    void identify();
}
