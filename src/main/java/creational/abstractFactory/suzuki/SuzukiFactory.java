package creational.abstractFactory.suzuki;

import creational.abstractFactory.car.Car;
import creational.abstractFactory.car.CarFactory;
import creational.abstractFactory.car.Moto;

public class SuzukiFactory implements CarFactory {

    @Override
    public Car getCar() {
        return new Swift();
    }

    @Override
    public Moto getMoto() {
        return new Gsxr();
    }
}
