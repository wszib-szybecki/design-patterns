package creational.abstractFactory.suzuki;

import creational.abstractFactory.car.Moto;

public class Gsxr implements Moto {

    @Override
    public void identify() {
        System.out.println("Suzuki GSXR");
    }
}
