package creational.abstractFactory.suzuki;

import creational.abstractFactory.car.Car;

public class Swift implements Car {

    @Override
    public void indentify() {
        System.out.println("Swift");
    }
}
