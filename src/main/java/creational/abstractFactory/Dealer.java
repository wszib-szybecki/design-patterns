package creational.abstractFactory;

import creational.abstractFactory.car.Car;
import creational.abstractFactory.car.CarFactory;
import creational.abstractFactory.car.Moto;

public class Dealer {

    private final CarFactory carFactory;

    public Dealer(CarFactory carFactory) {
        this.carFactory = carFactory;
    }

    public Car getCar() {
        return carFactory.getCar();
    }

    public Moto getMoto() {
        return carFactory.getMoto();
    }
}
