package creational.singleton;

public class Glass {

    private static Glass glass;

    static {
        Glass.glass = new Glass();
    }

    public static Glass getInstance() {
        return Glass.glass;
    }

    private Glass() {
    }
}
