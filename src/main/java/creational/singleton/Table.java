package creational.singleton;

public class Table {

    private static Table table;

    public static Table getInstance() {
        if (table == null) {
            table = new Table();
        }

        return table;
    }

    private Table() {
    }
}
