package creational.prototype.shape;

import creational.prototype.color.Color;

public interface Shape {

    Color getColor();
    Shape clone();
}
