package creational.prototype.shape;

import creational.prototype.color.Color;

public class RectangleShape implements Shape {

    private final Color color;

    public RectangleShape(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public Shape clone() {
        return new RectangleShape(color.clone());
    }
}
