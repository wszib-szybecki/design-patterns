package creational.prototype.shape;

import creational.prototype.color.Color;

public class TriangleShape implements Shape {

    private final Color color;

    public TriangleShape(Color color) {
        this.color = color;
    }

    @Override
    public Color getColor() {
        return this.color;
    }

    @Override
    public Shape clone() {
        return new TriangleShape(color.clone());
    }
}
