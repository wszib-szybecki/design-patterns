package creational.prototype.color;

public class BlueColor implements Color {

    @Override
    public String getColor() {
        return "Blue";
    }

    public Color clone() {
        return new BlueColor();
    }
}
