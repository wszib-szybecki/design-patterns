package creational.prototype.color;

public class RedColor implements Color {

    @Override
    public String getColor() {
        return "Red";
    }

    @Override
    public Color clone() {
        return new RedColor();
    }
}
