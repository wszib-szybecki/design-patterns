package creational.prototype.color;

public interface Color {

    String getColor();
    Color clone();
}
