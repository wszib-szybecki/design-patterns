package creational.prototype;

import creational.prototype.color.BlueColor;
import creational.prototype.color.RedColor;
import creational.prototype.shape.RectangleShape;
import creational.prototype.shape.Shape;
import creational.prototype.shape.TriangleShape;

public class Main {

    public static void main(String[] args) {
        Shape rectangle1 = new RectangleShape(new RedColor());
        Shape rectangle2 = rectangle1.clone();

        Shape triangle1 = new TriangleShape(new BlueColor());
        Shape triangle2 = triangle1.clone();

        System.out.println("Rect1: " + rectangle1);
        System.out.println("Rect2: " + rectangle2);

        System.out.println("Rect1.color: " + rectangle1.getColor());
        System.out.println("Rect2.color: " + rectangle2.getColor());

        System.out.println("Trai1: " + triangle1);
        System.out.println("Trai2: " + triangle2);

        System.out.println("Trai1.color: " + triangle1.getColor());
        System.out.println("Trai2.color: " + triangle2.getColor());
    }
}
