package creational.builder;

import java.util.Date;

public class User {

    private final String firstname;
    private final String lastname;
    private final Date birth;
    private final String race;
    private final double height;

    public User(String firstname, String lastname, Date birth, String race, double height) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.birth = birth;
        this.race = race;
        this.height = height;
    }

    public String getFirstname() {
        return firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public Date getBirth() {
        return birth;
    }

    public String getRace() {
        return race;
    }

    public double getHeight() {
        return height;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birth=" + birth +
                ", race='" + race + '\'' +
                ", height=" + height +
                '}';
    }
}
