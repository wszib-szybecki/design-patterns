package creational.builder;

import java.util.Date;

public class Main {

    public static void main(String[] args) {
        var user1 = new UserBuilder("Jan", "Kowalski").build();
        var user2 = new UserBuilder("Piotr", "Nowak").birth(new Date()).build();
        var user3 = new UserBuilder("Kamil", "Iksinski").race("white").build();

        System.out.println(user1.toString());
        System.out.println(user2.toString());
        System.out.println(user3.toString());
    }
}
