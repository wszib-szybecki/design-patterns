package creational.builder;

import java.util.Date;

public class UserBuilder {

    private final String firstname;
    private final String lastname;
    private Date birth;
    private String race;
    private double height;

    public UserBuilder(String firstname, String lastname) {
        this.firstname = firstname;
        this.lastname = lastname;
    }

    public UserBuilder birth(Date birth) {
        this.birth = birth;
        return this;
    }

    public UserBuilder race(String race) {
        this.race = race;
        return this;
    }

    public UserBuilder height(double height) {
        this.height = height;
        return this;
    }

    public User build() {
        return new User(firstname, lastname, birth, race, height);
    }
}
