package behavioral.chain;

public class Main {

    public static void main(String[] args) {
        Database database = new Database();

        Handler authChain = new UserExistsHandler(database)
                .setNextHandler(new ValidPasswordHandler(database));

        AuthService service = new AuthService(authChain);
        System.out.println(service.logIn("jan", "123456"));
        System.out.println(service.logIn("jan", "123457"));
    }
}
