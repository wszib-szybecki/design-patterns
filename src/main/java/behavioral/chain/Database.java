package behavioral.chain;

import java.util.HashMap;
import java.util.Map;

public class Database {

    private final Map<String, String> database;

    public Database() {
        this.database = new HashMap<>();
        this.database.put("jan", "123456");
        this.database.put("kamil", "6541123");
    }

    public boolean isValidUsername(String username) {
        return this.database.containsKey(username);
    }

    public boolean isValidPassword(String username, String password) {
        return this.database.get(username).equals(password);
    }
}
