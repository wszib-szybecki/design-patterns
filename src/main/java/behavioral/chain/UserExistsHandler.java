package behavioral.chain;

public class UserExistsHandler extends Handler {

    private final Database database;

    public UserExistsHandler(Database database) {
        this.database = database;
    }

    @Override
    public boolean handle(String username, String password) {
        if (!this.database.isValidUsername(username)) {
            System.out.println("User does not exist");
            return false;
        }

        return handleNext(username, password);
    }
}
