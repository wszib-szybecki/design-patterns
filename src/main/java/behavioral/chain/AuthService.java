package behavioral.chain;

public class AuthService {

    private final Handler handler;

    public AuthService(Handler handler) {
        this.handler = handler;
    }

    public boolean logIn(String username, String password) {
        if (this.handler.handle(username, password)) {
            System.out.println("User successfully logged in");
            // Do crazy stuff...

            return true;
        }

        return false;
    }
}
