package behavioral.chain;

public class ValidPasswordHandler extends Handler {

    private final Database database;

    public ValidPasswordHandler(Database database) {
        this.database = database;
    }

    @Override
    public boolean handle(String username, String password) {
        if (!this.database.isValidPassword(username, password)) {
            System.out.println("Bad user");
            return false;
        }

        return this.handleNext(username, password);
    }
}
