package behavioral.command;

public class Main {

    public static void main(String[] args) {
        Calculator calc = new Calculator();

        calc.execute(new AddCommand(10));
        System.out.println(calc.value);
        calc.undo();
        System.out.println(calc.value);

        calc.execute(new SubtractionCommand(20));
        System.out.println(calc.value);
        calc.undo();
        System.out.println(calc.value);

        calc.execute(new AddCommand(6));
        calc.execute(new MultipleCommand(6));
        System.out.println(calc.value);
        calc.undo();
        System.out.println(calc.value);

        calc.execute(new DivideCommand(2));
        System.out.println(calc.value);
        calc.undo();
        System.out.println(calc.value);
    }
}
