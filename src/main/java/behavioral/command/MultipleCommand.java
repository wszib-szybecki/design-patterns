package behavioral.command;

public class MultipleCommand implements Command {

    private final int value;

    public MultipleCommand(int value) {
        this.value = value;
    }

    @Override
    public int execute(int currentValue) {
        return currentValue * this.value;
    }

    @Override
    public int undo(int currentValue) {
        return currentValue / this.value;
    }
}
