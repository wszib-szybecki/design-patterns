package behavioral.command;

public class AddCommand implements Command {

    private final int value;

    public AddCommand(int value) {
        this.value = value;
    }

    @Override
    public int execute(int currentValue) {
        return this.value + currentValue;
    }

    @Override
    public int undo(int currentValue) {
        return currentValue - this.value;
    }
}
