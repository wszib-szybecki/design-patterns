package behavioral.command;

public interface Command {

    int execute(int currentValue);
    int undo(int currentValue);
}
