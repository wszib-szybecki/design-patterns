package behavioral.command;

import java.util.LinkedList;

public class Calculator {

    public int value = 0;
    private LinkedList<Command> history = new LinkedList<>();

    public void execute(Command command) {
        this.value = command.execute(this.value);
        this.history.push(command);
    }

    public void undo() {
        this.value = this.history.pop().undo(this.value);
    }
}
