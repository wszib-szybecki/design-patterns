package behavioral.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Movies00s implements MovieIterator {

    private final List<MovieInfo> bestMovies;

    public Movies00s() {
        this.bestMovies = new ArrayList<>();

        this.addMovie("The Ring", "Gore Verbinski", 2002);
        this.addMovie("Saw", "James Wan", 2004);
        this.addMovie("Twilight", "Catherine Hardwicke", 2008);
    }

    public void addMovie(String name, String director, int year) {
        MovieInfo movieInfo = new MovieInfo(name, director, year);

        this.bestMovies.add(movieInfo);
    }

    public List<MovieInfo> getBestMovies() {
        return bestMovies;
    }

    @Override
    public Iterator<MovieInfo> createIterator() {
        return this.bestMovies.iterator();
    }
}
