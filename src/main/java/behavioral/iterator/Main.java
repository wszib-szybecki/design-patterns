package behavioral.iterator;

public class Main {

    public static void main(String[] args) {
        Movies00s movies00s = new Movies00s();
        Movies10s movies10s = new Movies10s();
        Movies20s movies20s = new Movies20s();

        MoviePlaylist moviePlaylist = new MoviePlaylist(movies00s, movies10s, movies20s);
        moviePlaylist.showMovies();
    }
}
