package behavioral.iterator;

import java.util.Iterator;

public class MoviePlaylist {

    private final MovieIterator movies00s;
    private final MovieIterator movies10s;
    private final MovieIterator movies20s;

    public MoviePlaylist(MovieIterator movies00s, MovieIterator movies10s, MovieIterator movies20s) {
        this.movies00s = movies00s;
        this.movies10s = movies10s;
        this.movies20s = movies20s;
    }

    public void showMovies() {
        showMoviesInfo(this.movies00s.createIterator());
        showMoviesInfo(this.movies10s.createIterator());
        showMoviesInfo(this.movies20s.createIterator());
    }

    private void showMoviesInfo(Iterator<MovieInfo> iterator) {
        while (iterator.hasNext()) {
            showMovieInfo(iterator.next());
        }
        System.out.println();
    }

    private void showMovieInfo(MovieInfo movieInfo) {
        System.out.println("Name: " + movieInfo.getName());
        System.out.println("Director: " + movieInfo.getDirector());
        System.out.println("Year: " + movieInfo.getYear());
        System.out.println();
    }
}
