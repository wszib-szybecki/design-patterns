package behavioral.iterator;

import java.util.Arrays;
import java.util.Iterator;

public class Movies10s implements MovieIterator {

    private final MovieInfo[] bestMovies;
    private int key = 0;

    public Movies10s() {
        this.bestMovies = new MovieInfo[3];

        this.addMovie("How to train your dragon", "Christophera Sanders", 2013);
        this.addMovie("Arrival", "Denis Villeneuve", 2016);
        this.addMovie("Coco", "Lee Unkrich", 2017);
    }

    public void addMovie(String name, String director, int year) {
        MovieInfo movieInfo = new MovieInfo(name, director, year);

        this.bestMovies[key] = movieInfo;
        key++;
    }

    public MovieInfo[] getBestMovies() {
        return bestMovies;
    }

    @Override
    public Iterator<MovieInfo> createIterator() {
        return Arrays.asList(this.bestMovies).iterator();
    }
}
