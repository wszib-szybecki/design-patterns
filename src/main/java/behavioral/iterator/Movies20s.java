package behavioral.iterator;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class Movies20s implements MovieIterator {

    private final Map<Integer, MovieInfo> bestMovies;
    private int key = 0;

    public Movies20s() {
        this.bestMovies = new HashMap<>();

        this.addMovie("Dune", "Denis Villeneuve", 2021);
        this.addMovie("The Whale", "Darren Aronofsky", 2022);
        this.addMovie("Top Gun: Maverick", "Josepha Kosinski", 2022);
    }

    public void addMovie(String name, String director, int year) {
        MovieInfo movieInfo = new MovieInfo(name, director, year);

        this.bestMovies.put(key, movieInfo);
        key++;
    }

    public Map<Integer, MovieInfo> getBestMovies() {
        return bestMovies;
    }

    @Override
    public Iterator<MovieInfo> createIterator() {
        return this.bestMovies.values().iterator();
    }
}
