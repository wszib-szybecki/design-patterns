package behavioral.iterator;

import java.util.Iterator;

public interface MovieIterator {

    Iterator<MovieInfo> createIterator();
}
