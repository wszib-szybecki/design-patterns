package behavioral.iterator;

public class MovieInfo {

    private final String name;
    private final String director;
    private final int year;

    public MovieInfo(String name, String director, int year) {
        this.name = name;
        this.director = director;
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public String getDirector() {
        return director;
    }

    public int getYear() {
        return year;
    }
}
