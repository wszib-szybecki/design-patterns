package behavioral.state;

public class CashMachine {

    private final CashMachineState hasCardState;
    private final CashMachineState noCardState;
    private final CashMachineState correctPinState;
    private final CashMachineState outOfMoneyState;

    private CashMachineState currentState;
    private int cash = 2000;

    public CashMachine() {
        this.hasCardState = new HasCardState(this);
        this.noCardState = new NoCardState(this);
        this.correctPinState = new CorrectPinProvidedState(this);
        this.outOfMoneyState = new OutOfMoneyState(this);

        this.currentState = this.noCardState;
    }

    public void insertCard() {
        this.currentState.insertCard();
    }

    public void ejectCard() {
        this.currentState.ejectCard();
    }

    public void enterPin(int pin) {
        this.currentState.enterPin(pin);
    }

    public void withdrawCash(int cash) {
        this.currentState.withdrawCash(cash);
    }

    public CashMachineState getCurrentState() {
        return currentState;
    }

    public void setCurrentState(CashMachineState currentState) {
        this.currentState = currentState;
    }

    public int getCash() {
        return cash;
    }

    public void setCash(int cash) {
        this.cash = cash;
    }

    public CashMachineState getHasCardState() {
        return hasCardState;
    }

    public CashMachineState getNoCardState() {
        return noCardState;
    }

    public CashMachineState getCorrectPinState() {
        return correctPinState;
    }

    public CashMachineState getOutOfMoneyState() {
        return outOfMoneyState;
    }
}
