package behavioral.state;

public class HasCardState implements CashMachineState {

    private final CashMachine cashMachine;

    public HasCardState(CashMachine cashMachine) {
        this.cashMachine = cashMachine;
    }

    @Override
    public void insertCard() {
        System.out.println("You've already inserted card");
    }

    @Override
    public void ejectCard() {
        System.out.println("Card is ejected");
        cashMachine.setCurrentState(cashMachine.getNoCardState());
    }

    @Override
    public void enterPin(int pin) {
        if (pin == 1234) {
            System.out.println("Correct PIN");
            cashMachine.setCurrentState(cashMachine.getCorrectPinState());
        } else {
            System.out.println("Wrong PIN");
            System.out.println("Card is ejected");
            cashMachine.setCurrentState(cashMachine.getNoCardState());
        }
    }

    @Override
    public void withdrawCash(int cash) {
        System.out.println("You have to provide PIN first");
    }
}
