package behavioral.state;

public interface CashMachineState {

    void insertCard();
    void ejectCard();
    void enterPin(int pin);
    void withdrawCash(int cash);
}
