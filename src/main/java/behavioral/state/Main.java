package behavioral.state;

public class Main {

    public static void main(String[] args) {
        CashMachine cashMachine = new CashMachine();

        cashMachine.insertCard();
        cashMachine.ejectCard();
        cashMachine.insertCard();
        cashMachine.enterPin(1234);
        cashMachine.withdrawCash(2000);
        cashMachine.insertCard();
        cashMachine.enterPin(4321);
    }
}
