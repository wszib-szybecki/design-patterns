package behavioral.state;

public class CorrectPinProvidedState implements CashMachineState {

    private final CashMachine cashMachine;

    public CorrectPinProvidedState(CashMachine cashMachine) {
        this.cashMachine = cashMachine;
    }

    @Override
    public void insertCard() {
        System.out.println("Cannot insert another card");
    }

    @Override
    public void ejectCard() {
        System.out.println("Card is ejected");
        cashMachine.setCurrentState(cashMachine.getNoCardState());
    }

    @Override
    public void enterPin(int pin) {
        System.out.println("Already entered PIN");
    }

    @Override
    public void withdrawCash(int cash) {
        if (cash > this.cashMachine.getCash()) {
            System.out.println("Machine doesnt have that money");
            System.out.println("Your card is ejected");

            cashMachine.setCurrentState(cashMachine.getNoCardState());
        } else {
            System.out.println(cash + " is provided");
            cashMachine.setCash(cashMachine.getCash() - cash);

            System.out.println("Your card is ejected");
            cashMachine.setCurrentState(cashMachine.getNoCardState());
            if (cashMachine.getCash() == 0) {
                cashMachine.setCurrentState(cashMachine.getOutOfMoneyState());
            }
        }
    }
}
