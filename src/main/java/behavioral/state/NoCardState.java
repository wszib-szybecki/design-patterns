package behavioral.state;

public class NoCardState implements CashMachineState {

    private final CashMachine cashMachine;

    public NoCardState(CashMachine cashMachine) {
        this.cashMachine = cashMachine;
    }

    @Override
    public void insertCard() {
        System.out.println("Please enter your PIN");
        cashMachine.setCurrentState(cashMachine.getHasCardState());
    }

    @Override
    public void ejectCard() {
        System.out.println("Card is not inserted");
    }

    @Override
    public void enterPin(int pin) {
        System.out.println("Card is not inserted");
    }

    @Override
    public void withdrawCash(int cash) {
        System.out.println("Card is not inserted");
    }
}
