package behavioral.state;

public class OutOfMoneyState implements CashMachineState {

    private final CashMachine cashMachine;

    public OutOfMoneyState(CashMachine cashMachine) {
        this.cashMachine = cashMachine;
    }

    @Override
    public void insertCard() {
        System.out.println("We don't have any money");
        System.out.println("your card is ejected");
    }

    @Override
    public void ejectCard() {
        System.out.println("We don't have any money");
        System.out.println("There is no card to eject");
    }

    @Override
    public void enterPin(int pin) {
        System.out.println("We don't have any money");
    }

    @Override
    public void withdrawCash(int cash) {
        System.out.println("We don't have any money");
    }
}
